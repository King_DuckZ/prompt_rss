#!/usr/bin/env bash

ScriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$ScriptDir/rawdog_bash_variables.sh"

if [ -f "$RawdogStsFile" ]; then
	if [[ "$(head -1 "$RawdogStsFile")" == "0" ]]; then
		echo "1" > "$RawdogStsFile"
		echo " New feeds $(date '+%-H:%M')"
	fi
fi
