#!/usr/bin/env bash

ScriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$ScriptDir/rawdog_bash_variables.sh"

function getHash()
{
	md5sum "$1" | grep --colour=no --extended-regexp --only-matching '^\w{32}'
}

rawdog --no-lock-wait --update
rawdog --write

if [ ! -f "$RawdogOutFile" ]; then
	echo "rawdog output file \"${RawdogOutFile}\" not generated"
	exit 1
fi

if [ ! -f "$RawdogHshFile" ] && [ -f "$RawdogOutFile" ]; then
	getHash "$RawdogOutFile" > "$RawdogHshFile"
fi

if [ -f "$RawdogHshFile" ]; then
	if [[ $(cat "$RawdogHshFile") == $(getHash "$RawdogOutFile") ]]; then
		echo 1 > "$RawdogStsFile"
	else
		echo 0 > "$RawdogStsFile"
	fi
fi

if [ -f "$RawdogOutFile" ]; then
	getHash "$RawdogOutFile" > "$RawdogHshFile"
fi
