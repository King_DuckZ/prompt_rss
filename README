A simple project to let you update your prompt for a one-shot notification
when new rss feeds are available.

This project is intended for use with feeds that don't get updated very
frequently, for example a blog. It is based on rawdog
http://offog.org/code/rawdog/.

It is not extremely hard to get it in place, but you'll be required to do some
manual setup. It doesn't even have any particularly strong dependency on
rawdog, in fact the script is so simple you could rewrite it yourself!

I assumed you have set rawdog's output html file to be
~/.rawdog/rawdog_gen.html, by putting this in your ~/.rawdog/config:

outputfile rawdog_gen.html

You can then invoke the rawdogupdate.sh script from a cronjob, for example
Monday to Friday, every 30 minutes, like this:

15,45 * * * 1-5 $HOME/prompt_rss/rawdogupdate.sh

Last, you can invoke the showrawdognews.sh from your PS1 variable by putting
a line similar to the following in your .bashrc:

PS1='[\[\e[1;33m\]\u\[\e[0m\]@\h \[\e[33m\]\W\[\e[0m\]$($HOME/prompt_rss/showrawdognews.sh)]$ '
export PS1

That's it. Whenever there is an update, you can fire up lynx and read the news.
The cronjob updates rawdog's cache file, the wrapper script takes care of
comparing the old one with the new one and update a file in your ~/.rawdog
directory. If there are updates, the script invoked from within your PS1
variable will display a notification only once.
